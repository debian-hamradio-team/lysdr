#! /usr/bin/env python
# encoding: utf-8
# WARNING! All changes made to this file will be lost!

import os,platform
from waflib import Utils,Task
from waflib.TaskGen import feature,extension,after_method,before_method
from waflib.Tools.ccroot import link_task,stlink_task
class go(Task.Task):
	run_str='${GOC} ${GOCFLAGS} ${CPPPATH_ST:INCPATHS} -o ${TGT} ${SRC}'
class gopackage(stlink_task):
	run_str='${GOP} grc ${TGT} ${SRC}'
class goprogram(link_task):
	run_str='${GOL} ${GOLFLAGS} -o ${TGT} ${SRC}'
	inst_to='${BINDIR}'
	chmod=Utils.O755
class cgopackage(stlink_task):
	color='YELLOW'
	inst_to='${LIBDIR}'
	ext_in=['.go']
	ext_out=['.a']
	def run(self):
		src_dir=self.generator.bld.path
		source=self.inputs
		target=self.outputs[0].change_ext('')
		bld_dir=self.outputs[1]
		bld_dir.mkdir()
		obj_dir=bld_dir.make_node('_obj')
		obj_dir.mkdir()
		bld_srcs=[]
		for s in source:
			b=bld_dir.make_node(s.path_from(src_dir).replace(os.sep,'_'))
			b.parent.mkdir()
			try:
				try:os.remove(b.abspath())
				except Exception:pass
				os.symlink(s.abspath(),b.abspath())
			except Exception:
				b.write(s.read())
			bld_srcs.append(b)
			b.sig=Utils.h_file(b.abspath())
			pass
		cgo_flags_node=bld_dir.make_node('_waf_cgoflags.go')
		cgo_flags_node.write('''\
package %(target)s

/*
 #cgo CFLAGS: %(cgo_cflags)s
 #cgo LDFLAGS: %(cgo_ldflags)s
*/
import "C"
// EOF
'''%{'target':target,'cgo_cflags':' '.join(l for l in self.env['GOCFLAGS']),'cgo_ldflags':' '.join(l for l in self.env['GOLFLAGS']),})
		bld_srcs.insert(0,cgo_flags_node)
		cgo_flags_node.sig=Utils.h_file(cgo_flags_node.abspath())
		makefile_node=bld_dir.make_node("Makefile")
		makefile_tmpl='''\
# Copyright 2009 The Go Authors.  All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file. ---

include $(GOROOT)/src/Make.inc

TARG=%(target)s

CGOFILES=\\
\t%(source)s

include $(GOROOT)/src/Make.pkg

%%: install %%.go
	$(GC) $*.go
	$(LD) -o $@ $*.$O

'''%{'target':target.path_from(obj_dir),'source':' '.join([b.path_from(bld_dir)for b in bld_srcs])}
		makefile_node.write(makefile_tmpl)
		cmd=Utils.subst_vars('gomake ${GOMAKE_FLAGS}',self.env).strip()
		o=self.outputs[0].change_ext('.gomake.log')
		fout_node=bld_dir.find_or_declare(o.name)
		fout=open(fout_node.abspath(),'w')
		rc=self.generator.bld.exec_command(cmd,stdout=fout,stderr=fout,cwd=bld_dir.abspath(),)
		if rc!=0:
			import waflib.Logs as msg
			msg.error('** error running [%s] (cgo-%s)'%(cmd,target))
			msg.error(fout_node.read())
			return rc
		self.generator.bld.read_stlib(target,paths=[obj_dir.abspath(),],)
		tgt=self.outputs[0]
		if tgt.parent!=obj_dir:
			install_dir=os.path.join('${LIBDIR}',tgt.parent.path_from(obj_dir))
		else:
			install_dir='${LIBDIR}'
		self.generator.bld.install_files(install_dir,tgt.abspath(),relative_trick=False,postpone=False,)
		return rc
def compile_go(self,node):
	if not('cgopackage'in self.features):
		return self.create_compiled_task('go',node)
	bld_dir=node.parent.get_bld()
	obj_dir=bld_dir.make_node('_obj')
	target=obj_dir.make_node(node.change_ext('.a').name)
	return self.create_task('cgopackage',node,node.change_ext('.a'))
def go_compiler_is_foobar(self):
	if self.env.GONAME=='gcc':
		return
	self.source=self.to_nodes(self.source)
	src=[]
	go=[]
	for node in self.source:
		if node.name.endswith('.go'):
			go.append(node)
		else:
			src.append(node)
	self.source=src
	if not('cgopackage'in self.features):
		tsk=self.create_compiled_task('go',go[0])
		tsk.inputs.extend(go[1:])
	else:
		bld_dir=self.path.get_bld().make_node('cgopackage--%s'%self.target.replace(os.sep,'_'))
		obj_dir=bld_dir.make_node('_obj')
		target=obj_dir.make_node(self.target+'.a')
		tsk=self.create_task('cgopackage',go,[target,bld_dir])
		self.link_task=tsk
def go_local_libs(self):
	names=self.to_list(getattr(self,'use',[]))
	for name in names:
		tg=self.bld.get_tgen_by_name(name)
		if not tg:
			raise Utils.WafError('no target of name %r necessary for %r in go uselib local'%(name,self))
		tg.post()
		lnk_task=getattr(tg,'link_task',None)
		if lnk_task:
			for tsk in self.tasks:
				if isinstance(tsk,(go,cgopackage)):
					tsk.set_run_after(lnk_task)
					tsk.dep_nodes.extend(lnk_task.outputs)
			path=lnk_task.outputs[0].parent.abspath()
			self.env.append_unique('GOCFLAGS',['-I%s'%path])
			self.env.append_unique('GOLFLAGS',['-L%s'%path])
		for n in getattr(tg,'includes_nodes',[]):
			self.env.append_unique('GOCFLAGS',['-I%s'%n.abspath()])
		pass
	pass
def configure(conf):
	def set_def(var,val):
		if not conf.env[var]:
			conf.env[var]=val
	goarch=os.getenv('GOARCH')
	if goarch=='386':
		set_def('GO_PLATFORM','i386')
	elif goarch=='amd64':
		set_def('GO_PLATFORM','x86_64')
	elif goarch=='arm':
		set_def('GO_PLATFORM','arm')
	else:
		set_def('GO_PLATFORM',platform.machine())
	if conf.env.GO_PLATFORM=='x86_64':
		set_def('GO_COMPILER','6g')
		set_def('GO_LINKER','6l')
	elif conf.env.GO_PLATFORM in['i386','i486','i586','i686']:
		set_def('GO_COMPILER','8g')
		set_def('GO_LINKER','8l')
	elif conf.env.GO_PLATFORM=='arm':
		set_def('GO_COMPILER','5g')
		set_def('GO_LINKER','5l')
		set_def('GO_EXTENSION','.5')
	if not(conf.env.GO_COMPILER or conf.env.GO_LINKER):
		raise conf.fatal('Unsupported platform '+platform.machine())
	set_def('GO_PACK','gopack')
	set_def('gopackage_PATTERN','%s.a')
	set_def('CPPPATH_ST','-I%s')
	set_def('GOMAKE_FLAGS',['--quiet'])
	conf.find_program(conf.env.GO_COMPILER,var='GOC')
	conf.find_program(conf.env.GO_LINKER,var='GOL')
	conf.find_program(conf.env.GO_PACK,var='GOP')
	conf.find_program('cgo',var='CGO')

extension('.go')(compile_go)
feature('gopackage','goprogram','cgopackage')(go_compiler_is_foobar)
before_method('process_source')(go_compiler_is_foobar)
feature('gopackage','goprogram','cgopackage')(go_local_libs)
after_method('process_source','apply_incpaths',)(go_local_libs)