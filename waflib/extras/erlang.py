#! /usr/bin/env python
# encoding: utf-8
# WARNING! All changes made to this file will be lost!

from waflib import TaskGen
TaskGen.declare_chain(name='erlc',rule='${ERLC} ${ERLC_FLAGS} ${SRC[0].abspath()} -o ${TGT[0].name}',reentrant=False,ext_in='.erl',ext_out='.beam')
def configure(conf):
	conf.find_program('erlc',var='ERLC')
	conf.env.ERLC_FLAGS=[]
